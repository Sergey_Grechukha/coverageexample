package com.example.gsg.coverageexample.view;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.gsg.coverageexample.R;
import com.example.gsg.coverageexample.app.CoverageApp;
import com.example.gsg.coverageexample.contract.MainActivityContract;
import com.example.gsg.coverageexample.databinding.ActivityMainBinding;
import com.example.gsg.coverageexample.presenter.MainActivityPresenter;
import com.example.gsg.coverageexample.server.ServerApi;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements MainActivityContract {

    private MainActivityPresenter mPresenter;
    @Inject
    protected ServerApi mServerApi;
    private ActivityMainBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        ((CoverageApp) CoverageApp.provideAppContext()).dataComponent().inject(this);

        mPresenter = new MainActivityPresenter(mServerApi, this);
        mBinding.getIpBtn.setOnClickListener(v -> mPresenter.getIp());
    }

    @Override
    public void onSuccess(String origin) {
        mBinding.ipTv.setText(origin);
    }

    @Override
    public void onError(String error) {
        mBinding.ipTv.setText(error);
    }
}
